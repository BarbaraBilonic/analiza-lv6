﻿/*Napravite jednostavnu igru vješala.Pojmovi se učitavaju u listu iz datoteke,
 * i u svakoj partiji se odabire nasumični pojam iz liste. 
 * Omogućiti svu funkcionalnost koju biste očekivali od takve igre.
 * Nije nužno crtati vješala, dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo.*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vjesala
{
    public partial class Form1 : Form
    {
        List<string> rijeci = new List<string>();
        string path = "D:\\rijeci.txt";
        string rijec;
        int br_slova=0;
        int pokusaji=10;
        char[] prikaz_rijec = new char[50];
        






        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                
                string r;
                while ((reader.ReadLine()) != null)
                {
                    r = reader.ReadLine();
                    rijeci.Add(r);
                   
                }
            }
            Random rnd = new Random();
            int x = rnd.Next(0, rijeci.Count);
            rijec = rijeci[x];
            int br_slova = rijec.Length;
            for(int j = 0; j < rijec.Length; j++)
            {
                prikaz_rijec[j] = '*';
            }

            string prikaz = new string(prikaz_rijec);
            lb_rijec.Text = prikaz;
            lb_brslova.Text = (rijec.Length).ToString();
        }
     
       
     
       

        private void btn_izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_pokusaj_Click(object sender, EventArgs e)
        {
            int i;
            
            bool pogodak = false;
            char slovo;
            char.TryParse(tb_unos.Text, out slovo);
            for (i = 0; i < rijec.Length; i++)
            {
                if (rijec[i] == slovo)
                {

                    pogodak = true;
                    prikaz_rijec[i] = slovo;
                    string s = new string(prikaz_rijec);
                    lb_rijec.Text = s;
                    MessageBox.Show("slovo se nalazi u rijeci");
                    br_slova++;
                  


                }



            }
            if (pogodak == false && tb_unos.Text != null)
            {
                MessageBox.Show("slovo se ne nalazi u rijeci");
                pokusaji--;
                lb_brojp.Text = pokusaji.ToString();
                if (pokusaji == 0)
                {
                    MessageBox.Show("izgubili ste");
                    Application.Exit();
                }


            }
            if (br_slova == rijec.Length)
            {
                MessageBox.Show("POBJEDA!");
            }
        }
    }
}
