﻿namespace vjesala
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_unos = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lb_brojp = new System.Windows.Forms.Label();
            this.lb_rijec = new System.Windows.Forms.Label();
            this.btn_izlaz = new System.Windows.Forms.Button();
            this.lb_brslova = new System.Windows.Forms.Label();
            this.btn_pokusaj = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_unos
            // 
            this.tb_unos.Location = new System.Drawing.Point(126, 157);
            this.tb_unos.Name = "tb_unos";
            this.tb_unos.Size = new System.Drawing.Size(100, 22);
            this.tb_unos.TabIndex = 0;
            
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "broj pokusaja:";
            // 
            // lb_brojp
            // 
            this.lb_brojp.AutoSize = true;
            this.lb_brojp.Location = new System.Drawing.Point(165, 82);
            this.lb_brojp.Name = "lb_brojp";
            this.lb_brojp.Size = new System.Drawing.Size(24, 17);
            this.lb_brojp.TabIndex = 2;
            this.lb_brojp.Text = "10";
            // 
            // lb_rijec
            // 
            this.lb_rijec.AutoSize = true;
            this.lb_rijec.Location = new System.Drawing.Point(155, 259);
            this.lb_rijec.Name = "lb_rijec";
            this.lb_rijec.Size = new System.Drawing.Size(46, 17);
            this.lb_rijec.TabIndex = 3;
            this.lb_rijec.Text = "RIJEC";
            // 
            // btn_izlaz
            // 
            this.btn_izlaz.Location = new System.Drawing.Point(257, 396);
            this.btn_izlaz.Name = "btn_izlaz";
            this.btn_izlaz.Size = new System.Drawing.Size(84, 24);
            this.btn_izlaz.TabIndex = 4;
            this.btn_izlaz.Text = "Izlaz";
            this.btn_izlaz.UseVisualStyleBackColor = true;
            this.btn_izlaz.Click += new System.EventHandler(this.btn_izlaz_Click);
            // 
            // lb_brslova
            // 
            this.lb_brslova.AutoSize = true;
            this.lb_brslova.Location = new System.Drawing.Point(146, 304);
            this.lb_brslova.Name = "lb_brslova";
            this.lb_brslova.Size = new System.Drawing.Size(70, 17);
            this.lb_brslova.TabIndex = 5;
            this.lb_brslova.Text = "Broj slova";
            // 
            // btn_pokusaj
            // 
            this.btn_pokusaj.Location = new System.Drawing.Point(143, 201);
            this.btn_pokusaj.Name = "btn_pokusaj";
            this.btn_pokusaj.Size = new System.Drawing.Size(75, 23);
            this.btn_pokusaj.TabIndex = 6;
            this.btn_pokusaj.Text = "pokusaj";
            this.btn_pokusaj.UseVisualStyleBackColor = true;
            this.btn_pokusaj.Click += new System.EventHandler(this.btn_pokusaj_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 450);
            this.Controls.Add(this.btn_pokusaj);
            this.Controls.Add(this.lb_brslova);
            this.Controls.Add(this.btn_izlaz);
            this.Controls.Add(this.lb_rijec);
            this.Controls.Add(this.lb_brojp);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_unos);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_unos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_brojp;
        private System.Windows.Forms.Label lb_rijec;
        private System.Windows.Forms.Button btn_izlaz;
        private System.Windows.Forms.Label lb_brslova;
        private System.Windows.Forms.Button btn_pokusaj;
    }
}

