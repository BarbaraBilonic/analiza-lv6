﻿/*Napravite aplikaciju znanstveni kalkulator koja će imati funkcionalnost znanstvenog kalkulatora,
 * odnosno implementirati osnovne(+,-,*,/) i barem 5 naprednih(sin, cos, log, sqrt...) operacija.*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kalkulator
{
    public partial class Form1 : Form
    {
        int operacija = 0;
        double op1, op2;
        double rez;

        private void operand_TextChanged(object sender, EventArgs e)
        {
            if (operacija == 0)
            {
                double.TryParse(operand.Text, out op1);


            }
            else
            {
                double.TryParse(operand.Text, out op2);
                if (operacija == 4 && op2 == 0)
                {
                    operacija = 0;
                    MessageBox.Show("Ne moze se dieliti s 0");
                }

            }
        }

        private void plus_Click(object sender, EventArgs e)
        {
            operacija = 1;
        }

        private void minus_Click(object sender, EventArgs e)
        {
            operacija = 2;
        }

        private void puta_Click(object sender, EventArgs e)
        {
            operacija = 3;
        }

        private void podijeljeno_Click(object sender, EventArgs e)
        {
            operacija = 4;
        }

        private void korijen_Click(object sender, EventArgs e)
        {
            operacija = 5;
        }

        private void potencija_Click(object sender, EventArgs e)
        {
            operacija = 6;
        }

        private void logaritam_Click(object sender, EventArgs e)
        {
            operacija = 7;
        }

        private void sinus_Click(object sender, EventArgs e)
        {
            operacija = 8;
        }

        private void kosinus_Click(object sender, EventArgs e)
        {
            operacija = 9;
        }

        private void jednako_Click(object sender, EventArgs e)
        { 
                
            switch (operacija)
            {
                case 1: rez = op1 + op2; break;
                case 2: rez = op1 - op2; break;
                case 3: rez = op1 * op2; break;
                case 4: rez = op1 / op2; break;
                case 5: rez = Math.Sqrt(op1); break;
                case 6: rez = Math.Pow(op1, op2); break;
                case 7: rez = Math.Log(op1); break;
                case 8: rez = Math.Sin(op1); break;
                case 9: rez = Math.Sin(op1); break;


            }
            if (rez % ((int)rez) == 0)
                rezultat.Text = ((int)rez).ToString();
            else
                rezultat.Text = rez.ToString();
            operacija = 0;
        }

        private void izlaz_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public Form1()
        {
            InitializeComponent();
        }

    }
}
