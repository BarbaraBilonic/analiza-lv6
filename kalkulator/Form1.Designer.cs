﻿namespace kalkulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.puta = new System.Windows.Forms.Button();
            this.podijeljeno = new System.Windows.Forms.Button();
            this.korijen = new System.Windows.Forms.Button();
            this.potencija = new System.Windows.Forms.Button();
            this.logaritam = new System.Windows.Forms.Button();
            this.sinus = new System.Windows.Forms.Button();
            this.kosinus = new System.Windows.Forms.Button();
            this.izlaz = new System.Windows.Forms.Button();
            this.operand = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rezultat = new System.Windows.Forms.Label();
            this.jednako = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(32, 187);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(40, 40);
            this.plus.TabIndex = 0;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(32, 247);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(40, 40);
            this.minus.TabIndex = 1;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // puta
            // 
            this.puta.Location = new System.Drawing.Point(32, 308);
            this.puta.Name = "puta";
            this.puta.Size = new System.Drawing.Size(40, 40);
            this.puta.TabIndex = 2;
            this.puta.Text = "*";
            this.puta.UseVisualStyleBackColor = true;
            this.puta.Click += new System.EventHandler(this.puta_Click);
            // 
            // podijeljeno
            // 
            this.podijeljeno.Location = new System.Drawing.Point(32, 367);
            this.podijeljeno.Name = "podijeljeno";
            this.podijeljeno.Size = new System.Drawing.Size(40, 40);
            this.podijeljeno.TabIndex = 3;
            this.podijeljeno.Text = "/";
            this.podijeljeno.UseVisualStyleBackColor = true;
            this.podijeljeno.Click += new System.EventHandler(this.podijeljeno_Click);
            // 
            // korijen
            // 
            this.korijen.Location = new System.Drawing.Point(99, 187);
            this.korijen.Name = "korijen";
            this.korijen.Size = new System.Drawing.Size(40, 40);
            this.korijen.TabIndex = 4;
            this.korijen.Text = "sqrt";
            this.korijen.UseVisualStyleBackColor = true;
            this.korijen.Click += new System.EventHandler(this.korijen_Click);
            // 
            // potencija
            // 
            this.potencija.Location = new System.Drawing.Point(99, 247);
            this.potencija.Name = "potencija";
            this.potencija.Size = new System.Drawing.Size(40, 40);
            this.potencija.TabIndex = 5;
            this.potencija.Text = "^";
            this.potencija.UseVisualStyleBackColor = true;
            this.potencija.Click += new System.EventHandler(this.potencija_Click);
            // 
            // logaritam
            // 
            this.logaritam.Location = new System.Drawing.Point(99, 308);
            this.logaritam.Name = "logaritam";
            this.logaritam.Size = new System.Drawing.Size(40, 40);
            this.logaritam.TabIndex = 6;
            this.logaritam.Text = "log";
            this.logaritam.UseVisualStyleBackColor = true;
            this.logaritam.Click += new System.EventHandler(this.logaritam_Click);
            // 
            // sinus
            // 
            this.sinus.Location = new System.Drawing.Point(169, 187);
            this.sinus.Name = "sinus";
            this.sinus.Size = new System.Drawing.Size(40, 40);
            this.sinus.TabIndex = 7;
            this.sinus.Text = "sin";
            this.sinus.UseVisualStyleBackColor = true;
            this.sinus.Click += new System.EventHandler(this.sinus_Click);
            // 
            // kosinus
            // 
            this.kosinus.Location = new System.Drawing.Point(169, 247);
            this.kosinus.Name = "kosinus";
            this.kosinus.Size = new System.Drawing.Size(40, 40);
            this.kosinus.TabIndex = 8;
            this.kosinus.Text = "cos";
            this.kosinus.UseVisualStyleBackColor = true;
            this.kosinus.Click += new System.EventHandler(this.kosinus_Click);
            // 
            // izlaz
            // 
            this.izlaz.Location = new System.Drawing.Point(296, 384);
            this.izlaz.Name = "izlaz";
            this.izlaz.Size = new System.Drawing.Size(75, 23);
            this.izlaz.TabIndex = 9;
            this.izlaz.Text = "izlaz";
            this.izlaz.UseVisualStyleBackColor = true;
            this.izlaz.Click += new System.EventHandler(this.izlaz_Click);
            // 
            // operand
            // 
            this.operand.Location = new System.Drawing.Point(99, 109);
            this.operand.Name = "operand";
            this.operand.Size = new System.Drawing.Size(100, 22);
            this.operand.TabIndex = 10;
            this.operand.TextChanged += new System.EventHandler(this.operand_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(102, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "operand:";
            // 
            // rezultat
            // 
            this.rezultat.AutoSize = true;
            this.rezultat.Location = new System.Drawing.Point(293, 112);
            this.rezultat.Name = "rezultat";
            this.rezultat.Size = new System.Drawing.Size(55, 17);
            this.rezultat.TabIndex = 12;
            this.rezultat.Text = "rezultat";
            // 
            // jednako
            // 
            this.jednako.Location = new System.Drawing.Point(169, 308);
            this.jednako.Name = "jednako";
            this.jednako.Size = new System.Drawing.Size(40, 40);
            this.jednako.TabIndex = 13;
            this.jednako.Text = "=";
            this.jednako.UseVisualStyleBackColor = true;
            this.jednako.Click += new System.EventHandler(this.jednako_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 450);
            this.Controls.Add(this.jednako);
            this.Controls.Add(this.rezultat);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.operand);
            this.Controls.Add(this.izlaz);
            this.Controls.Add(this.kosinus);
            this.Controls.Add(this.sinus);
            this.Controls.Add(this.logaritam);
            this.Controls.Add(this.potencija);
            this.Controls.Add(this.korijen);
            this.Controls.Add(this.podijeljeno);
            this.Controls.Add(this.puta);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.plus);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button puta;
        private System.Windows.Forms.Button podijeljeno;
        private System.Windows.Forms.Button korijen;
        private System.Windows.Forms.Button potencija;
        private System.Windows.Forms.Button logaritam;
        private System.Windows.Forms.Button sinus;
        private System.Windows.Forms.Button kosinus;
        private System.Windows.Forms.Button izlaz;
        private System.Windows.Forms.TextBox operand;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label rezultat;
        private System.Windows.Forms.Button jednako;
    }
}

